﻿Hello Haskell!
==============

Фахртдинов Роберт Харисович

Для выполнения домашних работ нужно будет форкнуть репозиторий и после написания домашней сделать Pull Request

http://haskell.org
GHC

Haskell - ЯП, строго типизирован, с ленивыми вычислениями,
чисто функциональный

```
$ ghci
GHCi, version 7.10.2: http://www.haskell.org/ghc/  :? for help
Prelude> :t 1
1 :: Num a => a
Prelude> :t 1+1
1+1 :: Num a => a
Prelude> :t (+1)
(+1) :: Num a => a -> a
Prelude> :t map
map :: (a -> b) -> [a] -> [b]
Prelude> :t map (+1)
map (+1) :: Num b => [b] -> [b]
Prelude>
```

```
Prelude>product [1..10]
3628800
Prelude> let p = product [1..1000000000]
Prelude> p
Убито
```



